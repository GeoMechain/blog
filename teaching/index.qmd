---
title: "Teaching"
listing:
  - id: teaching_2022
    contents: teaching_2022.yml
    sort: "date desc"
    template: ../html/teaching/listing.ejs
page-layout: full
toc: true
toc-location: right
toc-title: Year
title-block-banner: true
---

## 2022

:::{#teaching_2022}
:::